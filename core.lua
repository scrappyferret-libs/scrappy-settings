--- Required libraries.
local json = require( "json" )
local mime
if system.getInfo( "platform" ) == "html5" or system.getInfo( "platform" ) == "nx64" then
	local base64 = require( ( _G.scrappyDir or "" ) .. "settings.base64" )
	mime =
	{
		b64 = base64.encode,
		unb64 = base64.decode
	}
else
	mime = require( "mime" )
end

-- Localised functions.
local open = io.open
local close = io.close
local lines = io.lines
local gsub = string.gsub
local find = string.find
local format = string.format
local pathForFile = system.pathForFile
local decode = json.decode
local encode = json.encode
local b64 = mime.b64
local unb64 = mime.unb64

-- Localised values
local ResourceDirectory = system.ResourceDirectory
local DocumentsDirectory = system.DocumentsDirectory
local platformSuffix = ( system.getInfo( "platform" ) == "macos" or system.getInfo( "platform" ) == "win32" or system.getInfo( "platform" ) == "html5" ) and "desktop"
platformSuffix = ( system.getInfo( "platform" ) == "ios" or system.getInfo( "platform" ) == "android" ) and "mobile" or platformSuffix
platformSuffix = ( system.getInfo( "platform" ) == "nx64" ) and "switch" or platformSuffix

--- Class creation.
local library = {}

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

	-- Set the separator we're going to be using
	self._separator = self._params.separator or ":"

	-- Table to hold the default settings
	self._default = {}

	-- Table to hold the changed settings
	self._changed = {}

	-- Load up the settings from disk
	self:_load()

end

--- Save out the current settings.
function library:save()
	if self._unsavedChanges then
		self:_writeFile()
	end
end

--- Gets a setting.
-- @param name The name of the setting to get.
-- @return The setting value, or nil if none found.
function library:get( name )
	return self._changed[ name ] == nil and self._default[ name ] or self._changed[ name ]
end

--- Sets a setting.
-- @param name The name of the setting to set.
-- @param value The new value.
-- @param dontSave True if you don't want to save out the new settings. Optional, defaults to false so it will save.
function library:set( name, value, dontSave )

	-- Flag the changes as unsaved ( if they are in fact changes )
	self._unsavedChanges = self._unsavedChanges or self._changed[ name ] ~= value
	
	-- Mark this new value in our changed list
	self._changed[ name ] = value

	-- Should we not avoid saving?
	if not dontSave then

		-- Then save
		self:save()

	end

end

--- Checks if a setting is a certain value.
-- @param name The name of the setting to check.
-- @param value The value to check for.
-- @return True if it is, false otherwise.
function library:is( name, value )
	return self:get( name ) == value
end

--- Resets a setting back to default.
-- @param name The name of the setting to reset. Can also be a table containing names. Optional, defaults to nil so the entire settings database is reset.
-- @param dontSave True if you don't want to save out the reset settings. Optional, defaults to false so it will save.
function library:reset( name, dontSave )

	-- Do we have a name passed in?
	if name then

		-- Was the name a table of other names?
		if type( name ) == "table" then

			-- Loop through them all
			for i = 1, #name, 1 do

				-- Resetting them, making sure we don't save on each one
				self:reset( name[ i ], true )

			end

		-- Or was the name just a regular string?
		elseif type( name ) == "string" then

			-- Reset this changed setting
			self._changed[ name ] = nil

		end


	else

		-- No name specified, so reset all settings
		self:_loadDefault()

	end

	-- Have we been told not to save?
	if not dontSave then

		-- If not then save the settings back out
		self:save()

	end

end

--- Load up all settings.
function library:_load()
	self:_loadDefault()
	self:_loadChanged()
end

--- Loads in the default settings.
function library:_loadDefault()

	-- Reset the changed settings
	self._changed = {}

	-- Load the default settings
	self._default = self:_readFile()

	-- Try to flip settings if we have a specific suffix
	if platformSuffix then

		-- Loop through all settings
		for k, v in pairs( self._default ) do

			-- Looking for any suffix specific defaults
			if find( k, "--" .. platformSuffix ) then

				-- And flip them over
				self._default[ gsub( k, "--" .. platformSuffix, "" ) ] = v

			end

		end

	end

end

--- Loads in the saved out settings, overwriting any default ones.
function library:_loadChanged()

	-- Load in the changed settings
	self._changed = self:_readFile( DocumentsDirectory )

end

--- Reads in the settings file.
-- @param baseDir The base directory of the file. Optiona, defaults to system.ResourceDirectory.
-- @return The read in settings.
function library:_readFile( baseDir )

	-- Create a path for the settings file
	local path = pathForFile( "settings.ini", baseDir or ResourceDirectory )

	-- Table to store the settings
	local settings = {}

	-- Do we have a path?
	if path then

		-- Open the file for reading
		local file, errorString = open( path, "r" )

		-- Do we have a file?
		if file then

			-- Loop through each line
			for line in lines( path ) do

				-- Table to store the line fields
				local fields = {}

				-- Split the line into two fields
				line:gsub( format( "([^%s]+)", self._separator ), function( c ) fields[ #fields + 1 ] = c end )

				-- Ensure we have two fields
				if #fields == 2 then

					-- Try to decode the line into json
					fields[ 2 ] = decode( fields[ 2 ] ) or fields[ 2 ]

					-- Is the field still a string?
					if type( fields[ 2 ] ) == "string" then

						-- Remove any trailing comma
						fields[ 2 ] = gsub( fields[ 2 ], ",", "" )

					end

					-- Convert strings to booleans if required
					if fields[ 2 ] == "true" then
						fields[ 2 ] = true
					end

					if fields[ 2 ] == "false" then
						fields[ 2 ] = false
					end

					-- Convert the strings to numbers if required
					fields[ 2 ] = tonumber( fields[ 2 ] ) or fields[ 2 ]

					-- Store out the setting by using the name as a key
					settings[ fields[ 1 ] ] = fields[ 2 ]

				end

			end

			-- Close the file
			close( file )

		end

	end

	-- Return the read in settings
	return settings

end

--- Writes out the settings file to the Documents directory.
function library:_writeFile()

	-- Create a path for the settings file
	local path = pathForFile( "settings.ini", DocumentsDirectory )

	-- Open the file for writing
	local file, errorString = open( path, "w" )

	-- Check we have a file
	if file then

		-- Table to store the settings for writing
		local settings = {}

		-- Loop through all the default settings
		for k, v in pairs( self._default ) do

			-- Storing them out
			settings[ k ] = v

		end

		-- Now loop through all the changed settings
		for k, v in pairs( self._changed ) do

			-- Storing them out, overwriting the default ones if they exist
			settings[ k ] = v

		end

		local settingsToSave = ""

		-- Loop through all the strings
		for k, v in pairs( settings or {} ) do

			-- Try to encode if needed
			v = encode( v ) or v

			-- Convert booleans to strings
			v = v == true and "true" or v
			v = v == false and "false" or v

			-- Append the setting
			settingsToSave = settingsToSave .. k .. self._separator .. v .. ",\n"

		end
		
		-- Write out the string
		file:write( settingsToSave )

		-- Close the file handle
		close( file )
		
		-- Flag the changes as saved
		self._unsavedChanges = false

	end

end

--- Exports all settings.
-- @return A json encoded table containing all settings.
-- @param b64Encode Should the settings be base 64 encoded? Optional, defaults to false.
function library:export( b64Encode )

	-- Store out the settings table
	local settings = {}

	-- Loop through the default settings
	for k, v in pairs( self._default or {} ) do

		-- Storing them out
		settings[ k ] = v

	end

	-- Loop through the changed settings
	for k, v in pairs( self._changed or {} ) do

		-- Storing them out
		settings[ k ] = v

	end

	-- Encode and return them
	settings = encode( settings )

	-- Should they be b64 encoded?
	if b64Encode then

		-- Then encode them
		settings = b64( settings )

	end

	-- Return the settings
	return settings

end

--- Imports some settings.
-- @param settings A json encoded table containing all the settings to import.
-- @param b64Decode Are the settings base 64 encoded? Optional, defaults to false.
function library:import( settings, b64Decode )

	-- Were some settings passed in?
	if settings then

		-- Should they be b64 decoded?
		if b64Decode then

			-- Then decode them
			settings = unb64( settings )

		end

		-- Decode them
		settings = decode( settings )

		-- Were the settings decoded properly?
		if settings and type( settings ) == "table" then

			-- Loop through the imported settings
			for k, v in pairs( settings ) do

				-- Setting them out
				self._changed[ k ] = v

			end

			-- Return as successful
			return true

		else

			-- Return as failed
			return false

		end

	end

	-- Return as failed
	return false

end

-- If we don't have a Scrappy Settings library
if not Scrappy.Settings then

	-- Then store the library out
	Scrappy.Settings = library

end

-- Return the new library
return library
